#include <iostream>
#include "include/Mat.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <iomanip>

using namespace MAT;

std::ostream& operator<<(std::ostream& os, const vec3& v)
{
	std::cout << "(" << v.x << " " << v.y << " " << v.z << ")";
	return os;
}

std::ostream& operator<<(std::ostream& os, const vec4& v)
{
	std::cout << "(" << v.x << " " << v.y << " " << v.z << " " << v.w << ")";
	return os;
}


inline float radians(double Angle)
{
	return (float)(Angle * M_PI / 180);
}

MAT::vec3 normalize(vec3 vec)
{
	float mag = std::sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
	if (mag > 0.0f)
	{
		float oneOverMag = 1.0f / mag;
		vec.x = vec.x * oneOverMag;
		vec.y = vec.y * oneOverMag;
		vec.z = vec.z * oneOverMag;
	}
	return vec;
}

MAT::mat4 rotate(mat4& I, float Angle, vec3& Axis)
{
	const float a = Angle;
	const float c = cos(a);
	const float s = sin(a);

	vec3 axis = normalize(Axis);
	vec3 temp((float(1) - c) * axis.x, (float(1) - c) * axis.y, (float(1) - c) * axis.z);


	mat4 Rotate;
	Rotate.matrixData[0] = c + temp.x * axis.x;

	Rotate.matrixData[1] = temp.x * axis.y - s * axis.z;//Rotate.matrixData[1] = temp.x * axis.y + s * axis.z;

	Rotate.matrixData[2] = temp.x * axis.z - s * axis.y;

	Rotate.matrixData[4] = temp.y * axis.x + s * axis.z;//Rotate.matrixData[4] = temp.y * axis.z - s * axis.z;

	Rotate.matrixData[5] = c + temp.y * axis.y;//
	Rotate.matrixData[6] = temp.y * axis.z + s * axis.x;

	Rotate.matrixData[8] = temp.z * axis.x + s * axis.y;
	Rotate.matrixData[9] = temp.z * axis.y - s * axis.x;
	Rotate.matrixData[10] = c + temp.z * axis.z;

	return  I * Rotate;

}

void unitNorm(quat& q)
{
	float angle = radians(q.s);
	q.v.normalize();
	q.s = cosf(angle * 0.5);
	q.v = q.v * sinf(angle * 0.5);
}

vec3 QRotation(vec3& vec,float uAngle, vec3&  uAxis)
{
	quat p(0, vec);
	uAxis.normalize();
	quat q(uAngle, uAxis);
	unitNorm(q);
	quat qInverse = q.inverse();
	quat rotateVector = q * p * qInverse;

	return rotateVector.v;

}


void show(mat4 m)
{
	int c = 0;
	for (int i = 0; i < 16; i++)
	{
		if (c == 4)
		{
			std::cout << std::endl;
			c = 0;
		}

		std::cout
			<< std::setw(10)
			<< std::setprecision(6)
			<< std::fixed
			<< m.matrixData[i] << " ";
		c++;
	}
	std::cout << std::endl;
}

void show(vec4 m)
{
	std::cout << std::setw(10) << std::setprecision(6) << std::fixed << m.x << " ";
	std::cout << std::setw(10) << std::setprecision(6) << std::fixed << m.y << " ";
	std::cout << std::setw(10) << std::setprecision(6) << std::fixed << m.z << " ";
	std::cout << std::setw(10) << std::setprecision(6) << std::fixed << m.w << " ";
	std::cout << std::endl;

}

vec4 operator*(vec4 vec,const mat4& m) 
{
	vec4 v;
	v.x = vec.x * m.matrixData[0] + vec.y * m.matrixData[4] + vec.z * m.matrixData[8] + vec.w * m.matrixData[12];
	v.y = vec.x * m.matrixData[1] + vec.y * m.matrixData[5] + vec.z * m.matrixData[9] + vec.w * m.matrixData[13];
	v.z = vec.x * m.matrixData[2] + vec.y * m.matrixData[6] + vec.z * m.matrixData[10] + vec.w * m.matrixData[14];
	v.w = vec.x * m.matrixData[3] + vec.y * m.matrixData[7] + vec.z * m.matrixData[11] + vec.w * m.matrixData[15];
	return v;
}

void RotationMatrix()
{
	MAT::vec4 Position{ 100, 0, 0, 1 };
	MAT::vec4 Direction{ 100, 0, 0, 0 };

	MAT::mat4 I = MAT::mat4();

	float Angle = radians(90.0f);

	MAT::vec3 Axis{ 0, 0, 1 };
	MAT::mat4 Rotation = rotate(I, Angle, Axis);

	show(Rotation);

	std::cout << std::endl;

	//Position = Rotation * Position;
	Position = Position * Rotation;
	show(Position);



	std::cout << std::endl;
	
	//Direction = Rotation * Direction;
	Direction = Direction * Rotation;
	show(Direction);

}

void Quaternions()
{

	MAT::vec3 Position{0, 1, 0};

	MAT::vec3 Axis{1, 0, 0};

	std::cout << Position;

	std::cout<<QRotation(Position, 90, Axis);

}

int main()
{
	//RotationMatrix();
	Quaternions();


	return 0;
}
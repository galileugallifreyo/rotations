#ifndef QUAT_H
#define QUAT_H
#include "vec3.h"


namespace MAT {

	class quat
	{

	public:

		float s;
		vec3 v;

		quat(float uS, vec3& uV);

		~quat();

		quat(const  quat& value);

		quat operator*(const quat& q)const;

		void operator*=(const quat& q);

		quat& operator=(const quat& value);

		void operator*=(const float value);

		quat inverse();

		quat conjugate();

		quat multiply(const quat& q)const;


		float norm();
		void normalize();


	};
}


#endif
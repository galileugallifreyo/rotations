#ifndef VEC4_H
#define VEC4_H



namespace MAT
{
	class vec4
	{
	private:

	public:
		float x;
		float y;
		float z;
		float w;

		vec4();
		vec4(float uX, float uY, float uZ, float uW);
		
		~vec4();

		vec4(const vec4& v);
		vec4& operator=(const vec4& v);


		void operator+=(const vec4& v);
		vec4 operator+(const vec4& v)const;
		void operator-=(const vec4& v);
		vec4 operator-(const vec4& v)const;
		void operator *=(const vec4& v);
		vec4 operator*(const float s)const;

		void operator/=(const vec4& v);
		vec4 operator/(const vec4& v)const;
		vec4 cross(const vec4& v)const;
		void operator %=(const vec4& v);
		vec4 operator%(const vec4& v)const;

		//vec4 operator*(const mat4& m)const;
		//vec4& operator*(const mat4& value);

		float magnitude();
		void normalize();


		
	};
}





#endif
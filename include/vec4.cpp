#include <iostream>
#include "vec4.h"
#include "mat4.h"
namespace MAT
{

	vec4::vec4() :x(0.0), y(0.0), z(0.0), w(0.0) {};
	vec4::vec4(float uX, float uY, float uZ, float uW) :x(uX), y(uY), z(uZ), w(uW) {}
	vec4::~vec4(){}

	vec4::vec4(const vec4& v) : x(v.x), y(v.y), z(v.z), w(v.w) {}
	
	vec4& vec4::operator=(const vec4& v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
		w = v.w;

		return *this;
	}

	void vec4::operator+=(const vec4& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		w += v.w;
	}

	vec4 vec4::operator+(const vec4& v)const
	{
		return vec4(x + v.x, y + v.y, z + v.z, w + v.w);
	}

	void vec4::operator-=(const vec4& v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		w -= v.w;
	}

	vec4 vec4::operator-(const vec4& v)const
	{
		return vec4(x - v.x, y - v.y, z - v.z, w - v.w);
	}

	void vec4::operator*=(const vec4& v)
	{
		x *= v.x;
		y *= v.y;
		z *= v.z;
		w *= v.w;
	}

	vec4 vec4::operator*(const float s)const
	{
		return vec4(s * x, s * y, s * z, s * w);
	}
	
	void vec4::operator/=(const vec4& v)
	{
		x /= v.x;
		y /= v.y;
		z /= v.z;
		z /= v.w;
	}

	vec4 vec4::operator/(const vec4& v)const
	{
		return vec4(x / v.x, y / v.y, z / v.z, w / v.w);
	}

	vec4 vec4::cross(const vec4& v)const
	{
		return vec4(y * v.z - z * v.y,
					z * v.x - x * v.z,
					x * v.y - y * v.x,
					1);
	}

	void vec4::operator%=(const vec4& v)
	{
		*this = cross(v);
	}

	vec4 vec4::operator%(const vec4& v)const
	{
		return vec4(y * v.z - z * v.y,
					z * v.x - x * v.z,
					x * v.y - y * v.x,
					1);
	}

	/*vec4 vec4::operator*(const mat4& m) const
	{
		vec4 v;
		v.x = x * m.matrixData[0] + y * m.matrixData[4] + z * m.matrixData[8] + w * m.matrixData[12];
		v.y = x * m.matrixData[1] + y * m.matrixData[5] + z * m.matrixData[9] + w * m.matrixData[13];
		v.z = x * m.matrixData[2] + y * m.matrixData[6] + z * m.matrixData[10] + w * m.matrixData[14];
		v.w = x * m.matrixData[3] + y * m.matrixData[7] + z * m.matrixData[11] + w * m.matrixData[15];
		return v;
	}*/

	float vec4::magnitude()
	{
		return std::sqrt(x * x + y * y + z * z);
	}

	void vec4::normalize()
	{
		float mag = std::sqrt(x * x + y * y + z * z);
		if (mag > 0.0f)
		{
			float oneOverMag = 1.0f / mag;
			x = x * oneOverMag;
			y = y * oneOverMag;
			z = z * oneOverMag;

		}
	}
}
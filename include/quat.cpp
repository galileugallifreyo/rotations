#include "quat.h"
#include "vec3.h"
#define _USE_MATH_DEFINES
#include <math.h>

namespace MAT
{

	quat::quat(float uS, vec3& uV) :s(uS), v(uV) {}

	quat::quat(const quat& value)
	{
		s = value.s;
		v = value.v;
	}

	void quat::operator*=(const quat& q)
	{
		(*this) = multiply(q);
	}

	quat quat::operator*(const quat& q)const
	{
		float scalar = s * q.s - v.dot(q.v);

		vec3 imaginary = q.v * s + v * q.s + v.cross(q.v);

		return quat(scalar, imaginary);

	}

	quat quat::multiply(const quat& q)const {

		float scalar = s * q.s - v.dot(q.v);

		vec3 imaginary = q.v * s + v * q.s + v.cross(q.v);

		return quat(scalar, imaginary);

	}


	quat& quat::operator=(const quat& value)
	{
		s = value.s;
		v = value.v;
		return (*this);
	}

	void quat::operator*=(const float value)
	{
		s *= value;
		v *= value;
	}

	quat quat::inverse()
	{

		float abs = norm();
		abs *= abs;
		abs = 1 / abs;
		quat conjugateValue = conjugate();

		float scalar = conjugateValue.s * abs;
		vec3 imaginary = conjugateValue.v * abs;
		return quat(scalar, imaginary);

	}

	quat quat::conjugate()
	{
		float scalar = s;
		vec3 imaginary = v * (-1);
		return quat(scalar, imaginary);
	}

	float quat::norm()
	{
		float scalar = s * s;
		float imaginary = v * v;
		return sqrt(scalar+imaginary);
	}

	void quat::normalize()
	{
		if (norm() != 0)
		{
			float normValue = 1 / norm();
			s *= normValue;
			v *= normValue;
		}
	}

	quat::~quat(){}


}